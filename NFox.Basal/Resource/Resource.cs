﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace NFox.Basal.Resource
{
    /// <summary>
    /// 资源类
    /// </summary>
    public class Resource
    {
        /// <summary>
        /// 从资源里读取字符串
        /// </summary>
        /// <param name="path">要在其中搜索资源的目录的名称。 path 可以是绝对路径或应用程序目录中的相对路径。</param>
        /// <param name="filename">资源的根名称。 例如，名为“MyResource.en-US.resources”的资源文件的根名称为“MyResource”</param>
        /// <param name="key">字符串的名字</param>
        /// <returns></returns>
        public static string GetStringFormResource(string path, string filename, string key)
        {
            ResourceManager rm = ResourceManager.CreateFileBasedResourceManager(filename, path, null);
            return rm.GetString(key);
        }

        /// <summary>
        /// 获取资源中的图片
        /// </summary>
        /// <param name="path">要在其中搜索资源的目录的名称。 path 可以是绝对路径或应用程序目录中的相对路径。</param>
        /// <param name="filename">资源的根名称。 例如，名为“MyResource.en-US.resources”的资源文件的根名称为“MyResource”</param>
        /// <param name="key">图片的名字</param>
        /// <returns></returns>
        public static Bitmap GetImageFormResource(string path, string filename, string key)
        {
            ResourceManager rm = ResourceManager.CreateFileBasedResourceManager(filename, path, null);
            return (Bitmap)rm.GetObject(key);
        }

        /// <summary>
        /// 创建资源文件
        /// </summary>
        /// <param name="sourcepath">存放资源(如图片，xml之类)的路径</param>
        /// <param name="destpath">生成资源文件的路径</param>
        /// <returns></returns>
        public static bool MakeResources(string sourcepath, string destpath)
        {
            if (sourcepath[sourcepath.Length - 1] != '\\')
            {
                sourcepath = sourcepath + "\\";
            }

            DirectoryInfo di = new DirectoryInfo(sourcepath);
            if (di == di.Root)
            {
                return false;
            }
            else
            {
                try
                {
                    foreach (FileInfo dllfile in di.GetFiles("*.dll"))
                    {
                        File.Copy(dllfile.FullName, Directory.GetParent(destpath).FullName + "\\" + dllfile.Name, true);
                    }
                }
                catch
                { }

                if (destpath[destpath.Length - 1] != '\\')
                {
                    destpath = destpath + "\\";
                }
                string filename = destpath + di.Name + ".resources";
                if (File.Exists(filename))
                {
                    File.Delete(filename);
                }

                ResourceWriter rw = new ResourceWriter(filename);
                foreach (FileInfo jpgfile in di.GetFiles("*.jpg"))
                {
                    System.Drawing.Image img = System.Drawing.Image.FromFile(jpgfile.FullName);
                    rw.AddResource(jpgfile.Name, img);
                }

                foreach (FileInfo xmlfile in di.GetFiles("*.xml"))
                {
                    string s = File.ReadAllText(xmlfile.FullName);
                    rw.AddResource(xmlfile.Name, s);
                }

                rw.Generate();
                rw.Close();

                return true;
            }
        }
    }
}
