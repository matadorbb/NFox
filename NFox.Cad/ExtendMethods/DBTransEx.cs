﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

namespace NFox.Cad
{
    /// <summary>
    /// 绘图空间
    /// </summary>
    public enum SPace
    {
        /// <summary>
        /// 当前空间
        /// </summary>
        Current,
        /// <summary>
        /// 模型空间
        /// </summary>
        Model,
        /// <summary>
        /// 图纸空间
        /// </summary>
        Paper
    }
    /// <summary>
    /// 事务管理扩展类
    /// </summary>
    public static class DBTransEx
    {
        /// <summary>
        /// 在指定的绘图空间里添加正交矩形
        /// </summary>
        /// <param name="tr">事务管理器</param>
        /// <param name="minPoint">左下角点坐标</param>
        /// <param name="maxPoint">右上角点坐标</param>
        /// <param name="space">空间枚举，默认为当前空间</param>
        /// <returns>矩形的id</returns>
        public static ObjectId DrawOrthogonalRectangle(this DBTransaction tr, Point3d minPoint, Point3d maxPoint,SPace space = SPace.Current)
        {
            var btr = space switch
            {
                SPace.Current => tr.OpenCurrentSpace(),
                SPace.Model => tr.OpenModelSpace(),
                SPace.Paper => tr.OpenPaperSpace(),
                _ => throw new NotImplementedException()
            };
            var pl = new Polyline();
            pl.Closed = true;
            pl.AddVertexAt(0, new Point2d(minPoint.X, minPoint.Y), 0, 0, 0);
            pl.AddVertexAt(1, new Point2d(maxPoint.X, minPoint.Y), 0, 0, 0);
            pl.AddVertexAt(2, new Point2d(maxPoint.X, maxPoint.Y), 0, 0, 0);
            pl.AddVertexAt(3, new Point2d(minPoint.X, maxPoint.Y), 0, 0, 0);
            return tr.AddEntity(btr,pl);
        }

        /// <summary>
        /// 在指定的绘图空间里添加真实矩形
        /// </summary>
        /// <param name="tr">事务管理器</param>
        /// <param name="basePt">矩形的基点</param>
        /// <param name="vec1">第一边的向量</param>
        /// <param name="vec2">第二边的向量</param>
        /// <param name="space">空间枚举，默认为当前空间</param>
        /// <returns>矩形的id</returns>
        public static ObjectId DrawRectangle(this DBTransaction tr, Point3d basePt, Vector3d vec1, Vector3d vec2, SPace space = SPace.Current)
        {
            var pt2 = basePt + (vec1 + vec2);
            var pt1 = basePt + vec1;
            var pt3 = basePt + vec2;
           
            var btr = space switch
            {
                SPace.Current => tr.OpenCurrentSpace(),
                SPace.Model => tr.OpenModelSpace(),
                SPace.Paper => tr.OpenPaperSpace(),
                _ => throw new NotImplementedException()
            };
            var pl = new Polyline();
            pl.Closed = true;
            pl.AddVertexAt(0, basePt.GetPoint2d(), 0, 0, 0);
            pl.AddVertexAt(1, pt1.GetPoint2d(), 0, 0, 0);
            pl.AddVertexAt(2, pt2.GetPoint2d(), 0, 0, 0);
            pl.AddVertexAt(3, pt3.GetPoint2d(), 0, 0, 0);
            return tr.AddEntity(btr, pl);
        }

        /// <summary>
        /// 在指定绘图空间添加直线
        /// </summary>
        /// <param name="tr">事务管理器</param>
        /// <param name="start">起点</param>
        /// <param name="end">终点</param>
        /// <param name="space">绘图空间，默认为当前空间</param>
        /// <param name="action">直线属性设置委托</param>
        /// <returns>直线的id</returns>
        public static ObjectId DrawLine(this DBTransaction tr, Point3d start, Point3d end,  Action<Line> action = null, SPace space = SPace.Current)
        {
            var btr = space switch
            {
                SPace.Current => tr.OpenCurrentSpace(),
                SPace.Model => tr.OpenModelSpace(),
                SPace.Paper => tr.OpenPaperSpace(),
                _ => throw new NotImplementedException()
            };
            var line = new Line(start, end);
            
            action?.Invoke(line);
            return tr.AddEntity(btr, line);
        }
    }
}
